﻿using System;
using System.Diagnostics;
using System.Linq;
using System.IO;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;

namespace Image2OLED.Core
{
    public static class Image2OLEDConverter
    {
        public static byte[] Convert(this byte[] pixelData, int width, int height, OLEDFormat oledFormat, int threshold = 200, bool inverse = false)
        {
            switch (oledFormat)
            {
                case OLEDFormat.MVLSB:
                    return pixelData.ConvertMVLSB(width, height, threshold, inverse);
                //case OLEDFormat.Unknown:
                default:
                    throw new NotImplementedException($"Converting to format {oledFormat} has yet to be implemented.");
            }
        }

        /// <summary>
        /// Gets the [human vision adjusted] pixel [luminosity] intensity.
        /// </summary>
        /// <param name="r">The red value.</param>
        /// <param name="g">The green value.</param>
        /// <param name="b">The blue value.</param>
        /// <param name="a">The alpha value.</param>
        /// <returns></returns>
        public static double GetPixelIntensity(double r, double g, double b, double a = 1.0)
            => (0.2126 * r + 0.7152 * g + 0.0722 * b) * a;

        public static byte GetPixelIntensity(byte r, byte g, byte b, byte a = 255)
            => (byte)Math.Round(GetPixelIntensity(r / 255.0, g / 255.0, b / 255.0, a / 255.0) * 255);

        public static byte GetPixelIntensity(this byte[] pixelBytes)
        {
            Debug.Assert(pixelBytes != null, "pixelBytes != null");
            switch (pixelBytes.Length)
            {
                case 0:
                    throw new Exception("GetPixelIntensity should not be called with an empty array");
                case 1:
                    // NB: Only one byte to the pixel, so just return this value
                    return pixelBytes[0];
                case 2:
                    throw new NotImplementedException("2 byte pixel data has yet to be defined and implemented.");
                case 3:
                    // RGB format
                    return GetPixelIntensity(pixelBytes[0], pixelBytes[1], pixelBytes[2]);
                case 4:
                    // RGBA format
                    return GetPixelIntensity(pixelBytes[0], pixelBytes[1], pixelBytes[2], pixelBytes[3]);
                default:
                    throw new NotImplementedException($"No supported has been implemented for pixel formats with {pixelBytes.Length} bytes");
            }
        }

        public static byte GetPixelIntensity<TPixelFormat>(this Image<TPixelFormat> image, int x, int y)
            where TPixelFormat : struct, IPixel<TPixelFormat>
        {
            var bytesPerPixel = image.PixelType.BitsPerPixel / 8;
            var pixelIndex = x + y * image.Width;
            var skipBytes = pixelIndex * bytesPerPixel;
            var pixelBytes = image.SavePixelData().Skip(skipBytes).Take(bytesPerPixel);
            return pixelBytes.ToArray().GetPixelIntensity();
        }

        public static byte[] GetPixelIntensities<TPixelFormat>(this Image<TPixelFormat> image)
            where TPixelFormat : struct, IPixel<TPixelFormat>
        {
            var intensities = new byte[image.Width * image.Height];
            for(var y = 0; y < image.Height; y++)
            for (var x = 0; x < image.Width; x++)
                intensities[x + y * image.Width] = image.GetPixelIntensity(x, y);
            return intensities;
        }

        public static byte[] Convert(this Image<Rgba32> image, OLEDFormat oledFormat, int threshold = 200, bool inverse = false)
            //=> image.SavePixelData().Convert(image.Width, image.Height, oledFormat, threshold, inverse);
            => image.GetPixelIntensities().Convert(image.Width, image.Height, oledFormat, threshold, inverse);

        public static string ConvertPy(this Image<Rgba32> image, OLEDFormat oledFormat, int threshold = 200,
            bool inverse = false)
            => image.Convert(oledFormat, threshold, inverse).ConvertPy();

        public static byte[] ConvertMVLSB(this byte[] pixelData, int width, int height, int threshold = 200, bool inverse = false)
        {
            var output = new byte[width * height / 8];

            for (var y = 0; y < height; y += 8)
            {
                for (var x = 0; x < width; x++)
                {
                    var b = 0;
                    for (var bit = 0; bit < 8; bit++)
                    {
                        var pixel = pixelData[x + (y + bit) * width];
                        if (inverse == pixel < threshold)
                            // If inverse and under threshold, or not inverse and over threshold
                            b |= 1 << bit;    // NB: |= was used in python source, but think it needs to be &=
                            //b &= (1 << bit);
                    }
                    // ReSharper disable once ArrangeRedundantParentheses
                    output[x + (y / 8) * width] = (byte) b;
                }
            }

            return output;
        }

        public static byte[] Convert(string filename, OLEDFormat oledFormat, int threshold = 200, bool inverse = false)
            => Image.Load(filename).Convert(oledFormat, threshold, inverse);

        public static byte[] Convert(Stream imageStream, OLEDFormat oledFormat, int threshold = 200,
            bool inverse = false)
            => Image.Load(imageStream).Convert(oledFormat, threshold, inverse);

        public static string GetBinaryString(this byte b) => $"\\x{b:X2}";

        public static string ConvertPy(this byte[] byteArray)
            => $"bytearray(b'{ string.Join("", byteArray.Select(GetBinaryString)) }')\r\n";

        public static string ConvertPy(string filename, OLEDFormat oledFormat, int threshold = 200,
            bool inverse = false)
            => Convert(filename, oledFormat, threshold, inverse).ConvertPy();

        public static string ConvertPy(Stream imageStream, OLEDFormat oledFormat, int threshold = 200,
            bool inverse = false)
            => Convert(imageStream, oledFormat, threshold, inverse).ConvertPy();
    }
}
