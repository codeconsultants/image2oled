﻿namespace Image2OLED.Core
{
    public enum OLEDFormat
    {
        Unknown = 0,
        /// <summary>
        /// The MVLSB format is Black and White, 1-bit per pixel, with 8 vertical pixels per byte/
        /// </summary>
        MVLSB = 1
    }
}
