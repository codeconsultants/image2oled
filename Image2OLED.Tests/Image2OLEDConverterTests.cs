using System.Linq;
using System.Runtime.InteropServices;
using Xunit;
using Image2OLED.Core;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;

namespace Image2OLED.Tests
{
    public class Image2OLEDConverterTests
    {
        [Fact]
        public void TestGetPixelIntensity1Byte()
        {
            var pixelData = new byte[] { 255 };
            var fullOn = Image2OLEDConverter.GetPixelIntensity(pixelData);
            pixelData = new byte[] { 100 };
            var belowThreshold = Image2OLEDConverter.GetPixelIntensity(pixelData);
            pixelData = new byte[] { 0 };
            var fullOff = Image2OLEDConverter.GetPixelIntensity(pixelData);
            Assert.Equal(255, fullOn);
            Assert.Equal(100, belowThreshold);
            Assert.Equal(0, fullOff);
        }
        [Fact]
        public void TestGetPixelIntensity3Byte()
        {
            var pixelData = new byte[] { 255, 255, 255 };
            var fullOn = Image2OLEDConverter.GetPixelIntensity(pixelData);
            pixelData = new byte[] { 100, 100, 100 };
            var belowThreshold = Image2OLEDConverter.GetPixelIntensity(pixelData);
            pixelData = new byte[] { 0, 0, 0 };
            var fullOff = Image2OLEDConverter.GetPixelIntensity(pixelData);
            Assert.Equal(255, fullOn);
            Assert.Equal(100, belowThreshold);
            Assert.Equal(0, fullOff);
        }
        [Fact]
        public void TestGetPixelIntensity4Byte()
        {
            var pixelData = new byte[] { 255, 255, 255, 255 };
            var fullOn = Image2OLEDConverter.GetPixelIntensity(pixelData);
            pixelData = new byte[] { 100, 100, 100, 128 };
            var belowThreshold = Image2OLEDConverter.GetPixelIntensity(pixelData);
            pixelData = new byte[] { 10, 10, 10, 0 };
            var fullOff = Image2OLEDConverter.GetPixelIntensity(pixelData);
            pixelData = new byte[] { 10, 10, 10, 200 };
            var alphad_dark = Image2OLEDConverter.GetPixelIntensity(pixelData);
            Assert.Equal(255, fullOn);
            Assert.Equal(50, belowThreshold);
            Assert.Equal(0, fullOff);
            Assert.Equal(8, alphad_dark);
        }

        [Fact]
        public void ConvertMVLSB1()
        {
            var image = new Image<Rgba32>(2, 8);
            for(var y = 0; y < image.Height; y++)
                for(var x = 0; x < image.Width; x++)
                    image.Frames.First()[x, y] = Rgba32.Black;

            var allBlack = Image2OLEDConverter.ConvertPy(image, OLEDFormat.MVLSB);
            image.Frames.First()[0, 7] = Rgba32.White;
            var whiteDot = Image2OLEDConverter.ConvertPy(image, OLEDFormat.MVLSB);

            for (var y = 0; y < image.Height; y++)
            for (var x = 0; x < image.Width; x++)
                image.Frames.First()[x, y] = Rgba32.White;
            var allWhite = Image2OLEDConverter.ConvertPy(image, OLEDFormat.MVLSB);

            Assert.Equal("bytearray(b'\\x00\\x00')\r\n", allBlack);
            Assert.Equal("bytearray(b'\\x80\\x00')\r\n", whiteDot);
            Assert.Equal("bytearray(b'\\xFF\\xFF')\r\n", allWhite);
        }
    }
}
