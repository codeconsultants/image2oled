# Image2OLED
## Initial Author: James Burton, Code-Consultants

This solution includes a core Net Standard library to handle converting images to micropython instructions to display on SSD1306 OLED displays, and associated website for online processing, and a CLI project to expose this for scripting.

## Usage

The simplest way to use this project is to visit the Azure hosted version at https://image2oled.azurewebsites.net, then upload a file and base from the provided example script.

Otherwise, the project should be cloned from BitBucket. and can be found here https://bitbucket.org/codeconsultants/image2oled/.  I am open to pull requests to master, but other users may create new branches if they wish to extend this solution (and feel free to include a Credits entry below when submitting).

## Version History

1.1 - 2018-06-18 - Added support for pixel-format command parameters, though no additional formats have yet been implemented.
1.0 - 2018-06-18 - Initial Release (functional proof-of-concept and guide)

## Credits

James Burton - Initial Author