﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Image2OLED.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration.UserSecrets;
using Image2OLED.Core;
using Microsoft.AspNetCore.Http;

namespace Image2OLED.Controllers
{
    public class HomeController : Controller
    {
        protected readonly IHostingEnvironment HostingEnvironment;
        public HomeController(IHostingEnvironment hostingEnvironment)
        {
            HostingEnvironment = hostingEnvironment;
        }

        [HttpGet]
        public IActionResult Index()
        {
            // Pick up TempData (if present) for display in the output panel
            ViewBag.Converted = TempData["Converted"];

            return View();
        }

        [HttpPost]
        public IActionResult Index(IFormCollection collection)
        {
            var file = collection.Files.Single();
            TempData["Converted"] = Image2OLEDConverter.ConvertPy(file.OpenReadStream(), OLEDFormat.MVLSB);
            return Index();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
