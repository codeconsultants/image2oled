﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Image2OLED.Core;

namespace Image2OLED.CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            var output = new StringBuilder();
            var argList = args.ToList();

            var inverse = false;
            var threshold = 200;
            var format = OLEDFormat.MVLSB;

            var error = false;

            // Process inverse flag(s)
            var inverseArgs = argList.Where(arg => arg == "-i" || arg == "--inverse").ToList();
            while (inverseArgs.Any())
            {
                var arg = inverseArgs.First();
                argList.Remove(arg);
                if (inverse)
                    output.AppendLine("Ignoring duplicate -i or --inverse option parameter");
                inverse = true;
            }

            // Process threshold flag(s)
            var thresholdArgs = argList.Where(arg => arg.StartsWith("-t=") || arg.StartsWith("--threshold=")).ToList();
            while (thresholdArgs.Any())
            {
                var arg = thresholdArgs.First();
                thresholdArgs.Remove(arg);
                threshold = int.Parse(arg.Substring(arg.IndexOf("=", StringComparison.Ordinal) + 1));
            }
            
            // Process [output pixel] format flag(s)
            var formatArgs = argList.Where(arg => arg.StartsWith("-f=") || arg.StartsWith("--format=")).ToList();
            while (formatArgs.Any())
            {
                var arg = formatArgs.First();
                formatArgs.Remove(arg);
                var formatName = arg.Substring(arg.IndexOf('=') + 1);
                switch (formatName)
                {
                    case "MVLSB":
                        format = OLEDFormat.MVLSB;
                        break;
                    default:
                        error = true;
                        output.AppendLine($"Unknown format specified: {formatName}");
                        break;
                }
            }

            if(!error) switch (argList.Count)
            {
                case 1:
                    GenerateAndSave(output, argList[0], GenerateOutputName(argList[0]), format, inverse, threshold);
                    break;
                case 2:
                    GenerateAndSave(output, argList[0], argList[1], format, inverse, threshold);
                    break;
                default:
                    output.AppendLine("Invalid syntax:");
                    error = true;
                    break;
            }

            if (error)
            {
                output.AppendLine("USAGE: image2oled [options...] <sourceImagePath> [<outputPath>]");
                output.AppendLine("OPTIONS:");
                output.AppendLine("-i | --inverse   => Inverse output");
                output.AppendLine("-t=<threshold> | --threshold=<threshold>     => Set cut-off luminosity value (1-255)");
                output.AppendLine("-f=<outputFormat> --format=<outputFormat>      => Sets output pixel format (MVLSB|...)");
            }

            Console.WriteLine(output.ToString());
        }

        static string GenerateAndSave(StringBuilder consoleOutput, string source, string outputFilename, OLEDFormat format = OLEDFormat.MVLSB, bool inverse = false, int threshold = 200)
        {
            consoleOutput.AppendLine($"Converting image {source}");
            consoleOutput.AppendLine($"Saving to {outputFilename}");
            var pyScript = Image2OLEDConverter.ConvertPy(source, format, inverse: inverse, threshold: threshold);
            consoleOutput.AppendLine(pyScript);
#pragma warning disable SEC0112 // Path Tampering Unvalidated File Path
            File.WriteAllText(outputFilename, pyScript);
#pragma warning restore SEC0112 // Path Tampering Unvalidated File Path
            return pyScript;
        }

        static string GenerateOutputName(string inputName) => $"{inputName}.py";
    }
}
